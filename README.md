Made
====

**Notice:** This is in process to be ported to
[Rust](https://www.rust-lang.org/)

Automating tasks like compiling markdown or latex documents can be neatly
handled using [make](https://www.gnu.org/software/make/), but you still need to
write the appropriate *Makefile*'s.

**Made** keep a set of *Makefile*'s for very common tasks allowing you to reduce
boilerplate.


Install
-------

Install with:

        $ sudo make install

Uninstall with:

        $ sudo make uninstall


Using
-----
